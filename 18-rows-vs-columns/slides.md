

# ROWS vs COLUMNS


<br>

Rappels :

		* mode de stockage

		* facilité de lectures/écritures (volumes)
				* SSD vs HDD

		* les datas sont stockées sous blocks contigus (chunks)

		* attention d'autres paramètres à rendre en compte :
				* indexation
				* distribution

--------------------------------------------

# ROWS vs COLUMNS

ROWS 

<br>

		* organisé par enregistrement (ligne d'enregistrement)

```
F1A | F2A | F3A | F1B | F2B | F3B...
```

Note : éventuellement différents fichiers (datafiles)

--------------------------------------------

# ROWS vs COLUMNS

ROWS 

<br>

		* une ligne = un enregistrement > stockée dans les chunks

		* ex : mysql, mariadb, postgresql...

--------------------------------------------

# ROWS vs COLUMNS

ROWS 

Opérations :

<br>

		* plus de données scannées (+IO même si inutile)

		* mais toutes les datas d'une ligne une fois trouvé
			* bon pour un select *
			* ou un where

<br>

		* lecture/écriture individuelles efficaces par enregistrement

<br>

		* ajout simple et rapide en fin de datafile

<br>

		* update : facile !!!

		* mais inefficace dans une approche analyse de colonne

--------------------------------------------

# ROWS vs COLUMNS

ROWS 

<br>

		* communément utilisées en OLTP :
				* OLTP : Online Transactional Processing
				* vs OLAP : Online Analytical Processing (Analytics)
				* efficace sur une transaction (insert, update...)

--------------------------------------------

# ROWS vs COLUMNS

COLUMNS

<br>

		* regroupement des datas d'une même colonne dans le datafile

```
F1A | F1B | F1C | F2A | F2B | F2C...
```

		* ex : cassandra, clickhouse, snowflake... et des TSDB

--------------------------------------------

# ROWS vs COLUMNS

COLUMNS

<br>

Opérations :

		* facile si un travail sur une colonne

<br>

		* si une colonne > moins de block (IO)

<br>

		* mais si plusieurs colonnes (compliqué, * impossible)

<br>

		* mais si plusieurs colonnes (compliqué, * impossible)

<br>

		* difficile de where sur F1 et récupérer F2

		* si nb colonnes faibles > C-Store

--------------------------------------------

# ROWS vs COLUMNS

COLUMNS

<br>

		* communément utilisées en OLTP :
				* OLAP : Online Analytical Processing (Analytics)
				* efficace sur une transaction (sum, count, tsdb...)

