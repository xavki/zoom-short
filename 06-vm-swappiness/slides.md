

# SYSCTL - VM.SWAPPINESS


<br>

* déclencheur du swap : 1 - 100

* valeur par défaut : 60 (ubuntu, debian...)

<br>

* signification : mémoire libre restante en %
		* 60 > reste 60% de RAM dispo
		* 30 > 70% de la RAM utilisée
		* 10 > 90% de la RAM est utilisée

<br>

* valeur à retrouver dans :
		* /proc/sys/vm/swappiness
		* sysctl vm.swappiness

<br>

* pour modifier :
		* temporairement (perdu au reboot) : sysctl -w vm.swappiness=10
			* ou edit /proc/sys/vm/swappiness
		* permanent : /etc/sysctl.conf (ou sous fichier)

<br>

* attention 0 > ne veut pas dire pas de swap
		* swapoff -a

---------------------------------------------------------------------------

# SYSCTL - VM.SWAPPINESS


<br>

* purge de la swap : 
		* swapoff/swapon

<br>

* quelques exemples :

		* Oracle/Hadoop > 10

		* Mariadb > 1

		* Couchbase > 1 - 0

---------------------------------------------------------------------------

# SYSCTL - VM.SWAPPINESS


<br>

* pour quoi est utilisée la machine :

		* forte utilisation de RAM (bdd nosql)

		* applicatif supportant les OOMKill (multiple instances...)

		* capacité de l'applicatif à bien gérer la RAM (autonome = 0 - 1 - 10)

