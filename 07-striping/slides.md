

# LVM - STRIP - IOPS


<br>

LVM :

* Physical Volume (PV)  >  Volume Group (VG)  >  Logical Volume (LV)

<br>

DISQUES :

* PV = représentation physique de disque

* 1 caractéristique de disque > Opérations par seconde (IOPS)
		* lecture/écriture
		* HDD / SSD

-------------------------------------------------------------------------

# LVM - STRIP

<br>

LV :

* comportement classique : PV1 puis PV2...

* lectures/écritures sur un seul disque à la fois

>> pas de parallélisation : PV = 100 IOps > max = 100 IOps

<br>

STRIP :

* parallélisation : PV1 ~ PV2

* PV = 100 IOps > max ~ 200 IOps

```
lvcreate -n lv2 -L 1G -i 2 vg2
lvdisplay -m lv2
```

Attention : extension de 2PV => 4PV / 4PV => 8PV


... on en reparle dans un prochain 2min

