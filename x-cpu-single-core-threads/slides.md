

# GITOPS ???


<br>

## OUTIL DE VERSIONNING

* système de contrôle de version

* changement de code / versionning

* travail en équipe

* les flows (gitflow...)

* dépôt git = source de vérité

* évite/interdit tout changement manuel

* ne plus se rendre sur les machines !!!

* remise en état rapide : backup (hors datas)

* descriptif > comprendre et décrire une infrastructure

Référence : Git

-----------------------------------------------------------------------------------

## PIPELINE

1. développement du code sur une branche feature/dev

2. test de la branche (plus ou moins)

3. merge/pull request + relecture (mainteneur)

4. merge de la branche dans une branche de déploiement (trigger ou non)

5. application des modifications

Plusieurs modes :
		* push
		* pull
 

------------------------------------------------------------------------------------

## MODE PUSH

<br>

* push : assez classique (ansible)

* ou mélange de rôle : orchestration / planificateur de tâches

* outil externe lance une tâche (ex : jenkins)

* trigger ou non

* récupération de code source

-----------------------------------------------------------------------------------

## MODE PULL

<br>

* pull : système à base d'agent (puppet, salt...)

* revenu au goût du jour avec kubernetes

* agent/outil > synchronise l'état local avec dépôt git

* agent/outil est dans le system

* kubernetes > flux


