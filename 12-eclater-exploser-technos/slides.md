

# LES TECHNOS : s'éclater ou exploser ??!!


<br>

L'OPENSOURCE c'est cool !!! et le LIBRE aussi !!!

<br>

	* permet l'innovation

<br>

	* permet la transparence (en partie...)

<br>

	* permet la révélaion de communauté

<br>

	* permet d'éviter l'isolement complet

-----------------------------------------------------------------------------------

# LES TECHNOS : s'éclater ou exploser ??!!

<br>

Les TECHNOLOGIES font vivre des entreprises > femmes/hommes

<br>

	* les infrastructures doivent être pérennes

<br>

	* l'ensemble doit être sécurisé (à de nombreux niveaux)

<br>

	* la maintenance joue un rôle essentiel (simplicité et efficacité)

<br>

	* les choix des technologies sont cruciaux en fonction des ressources


-----------------------------------------------------------------------------------

# LES TECHNOS : s'éclater ou exploser ??!!

<br>

PATIENCE et SIMPLICITE

<br>

		* une production se complexifie par elle même

<br>

		* ne pas chercher à être le premier :
				* à mettre à jour
				* à utiliser une tehcno

<br>

		* faire le choix des technologies qui ont fait leurs preuves

<br>

		* filtrer sa veille et tenter de discerner le consommable/durable

-----------------------------------------------------------------------------------

# LES TECHNOS : s'éclater ou exploser ??!!

<br>

		* juger de la maintenabilité vis à vis de vos ressources
				* humaines
				* financières
				* compétences

<br>

		* juger de la maintenabilité vis à vis de la communauté en question
				* derniers/nombre de commits
				* personnes référentes
				* la documentation, les articles de blogs, trouver des réponses

<br>

		* en réglant un problème on en génère d'autres ??

<br>

		* est-ce qu'on le fait pour se faire plaisir ou par utilité ?

-----------------------------------------------------------------------------------

# LES TECHNOS : s'éclater ou exploser ??!!

<br>

Poser vous des règles dans votre VEILLE également !!!

<br>

		* identifier les standards, les leaders et leurs concurrents

<br>

		* laisser mûrrir les technos avant de vous jeter dedans

<br>

		* au mieux couvrez les standards de chaque type
				* monitoring
				* logging
				* infra as code
				* sécurité...

<br>

		* comprendre une techno plutôt que l'apprendre aide à trier


-----------------------------------------------------------------------------------

# LES TECHNOS : s'éclater ou exploser ??!!

<br>

L'exemple KUBERNETES

<br>

		* beaucoup de sociétés le choississent sans forcément une nécessité

<br>

		* son choix entraîne d'autres choix techniques et des migrations

<br>

		* la jungle de technos liées est démentielle (ne pas s'éparpiller)

<br>

		* discerner les standards (métriques, logs...)


