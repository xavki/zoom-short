

# LA SWAP


<br>

* fait partie de la mémoire virtuelle

	mémoire virtuelle = RAM (Random Access Memory) + SWAP (disque)

Note : 10 x SSD  /  50 x HDD

<br>

* communément :

		* pas assez de RAM

		* la SWAP permet de déborder sur disque

		* lenteur lecture/écriture (I/O)

<br>

* commandes

```
free -h
top
vmstat
...
```

----------------------------------------------------------

# LA SWAP

<br>

* parfois on recommande la désactivation (plus facile à diagnostiquer)

```
swapoff
vim /etc/fstab
```

<br>

* facilité à swaper définie par `vm.swappiness`
		* valeur de 1 à 100 (défaut = 60)
		* pour centage de RAM restant disponible
		* /etc/sysctl.conf (.d)
		* `sysctl -w vm.swappiness=10`

<br>

ATTENTION : 

		* le SWAP n'est pas que du débordement

		* une machine avec de la RAM dispo peut swapper

		* en fonction de la fréquence d'utilisation des datas

Note : en lien avec un autre paramètre le cache
