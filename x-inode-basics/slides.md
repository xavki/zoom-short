

# LVM - STRIP / CHUNKS


<br>

STRIP

```
lvcreate -n lv2 -L 1G -i 2 vg2
```

<br>

CHUNKS 

Datas stockées en morceaux = chunks

Chunks > potentiellement sur différents disques

>> composant important de la parallélisation

--------------------------------------------------------------------

## SIZE


<br>

SIZE - CHUNKS

```
lvcreate -n lv2 -L 1G -i 2 -I 128k vg2
```

Default : 64kB

<br>

Deux principaux cas par rapport aux tailles :

	* chunks > requête 
			* beaucoup de petites requêtes

	* chunks < requête
			* peu de requêtes mais grosses

--------------------------------------------------------------------

## SIZE

<br>

TEST

```
fio 
--randrepeat=1 
--name=randrw
--rw=randrw
--direct=1
--ioengine=libaio
--bs=128k
--numjobs=10
--size=512M
--runtime=60
--time_based
--iodepth=64
--group_reporting
```
