%title: ArgoEvents
%author: xavki
%Vidéos: [ArgoEvents]()
%blog: [Xavki Blog](https://xavki.blog)


# Projet ARGO


<br>

Projet Argo - Kubernetes

Site : https://argoproj.github.io/


---------------------------------------------------------------------

# Projet ARGO


		* workflow : orchestration de job k8s
				* multiples étapes
				* parallelisation
				* machine learning...



---------------------------------------------------------------------

# Projet ARGO


<br>

		* continuous deployment (CD) : gitops deploiement
				* deploiement
				* configuration

---------------------------------------------------------------------

# Projet ARGO

<br>

		* rollout : stratégies de déploiements
				* blue/green
				* canary deploiement
				* ...
				* traffic shifting
				* metrics

---------------------------------------------------------------------

# Projet ARGO

<br>

		* events : déclenchement de ressources k8s sur évènements
				* connexion à des mqtt, webhook etc
				* évènement cloud
				* option > trigger des outils externes k8s
				


