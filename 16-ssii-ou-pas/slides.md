

# SSII ou pas ?


<br>

POUR

* facilité à donner la chance (ex : devops...)

<br>

* se créer une expérience (moins de projets perso...)

<br>

* voir différentes infrastructures (attention à la taille)

<br>

* travailler avec des collègues différents (encore que...)

<br>

* les salaires

<br>

* entrée et recrutement d'entreprises (internalisation)

<br>

* avec le télétravail moins de risque de localisation

<br>

* passer des certifications / diplomes...

-----------------------------------------------------------

# SSII ou pas ?


CONTRE

<br>

* l'environnement des grands groupes & SSII lands

<br>

* l'approche technique des grands groupes

<br>

* le salaire (bascule dans les clients finaux difficiles)

<br>

* parfois contraints sur les délais (faire vite...)

<br>

* l'ambiance de passionnés chez le client

<br>

* ce qui compte c'est chez le client (même si cool)

<br>

* tu restes un point à marger (des choses peuvent t'échapper)

<br>

* passer des certifications


---------------------------------------------------------


MON AVIS

<br>

* c'est bien si à taille humaine

<br>

* dans des entreprises à taille humaine

<br>

* avec système de mentorat ou travail en groupe

<br>

* c'est pas fait pour moi 

* besoin de toucher à pas mal de techno avec autonomie

* bien au début mais atteint des limites pour la carrière

* progession technique aléatoire

