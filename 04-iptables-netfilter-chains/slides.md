

# CHAINS & NETWORK STACK


<br>

```
    NETWORK                         KERNEL SPACE                        USER SPACE

<------------------->  <-------------------------------------->     <------------------>


                       +--------------------------------------+
                 eth0  |                                      |
                       |                                      |      +----------------+
 +------------>   +----+                                      |      |                |
                  |    |                                      |      |                |
 <------------+   +----+                                      |      |                |
                       |             +-----------+            |      |    ./app.py    |
                       |             |           |            |      |                |
                       |             |  Routes   |            |      |                |
                       |             |           |            |      |                |
                 eth1  |             |           |            |      +----------------+
                       |             +-----------+            |
                  +----+                                      |
 <------------+   |    |                                      |
                  +----+                                      |
                       |                                      |
                       |                                      |
                       +--------------------------------------+
```

----------------------------------------------------------------------------------------------------------------

## PREROUTING

<br>


```
    NETWORK                         KERNEL SPACE                        USER SPACE

<------------------->  <-------------------------------------->     <------------------>


                       +--------------------------------------+
                 eth0  |                                      |
       1               |                                      |      +----------------+
 +------------>   +----+     PREROUTING                       |      |                |
                  |    +------------------+                   |      |                |
 <------------+   +----+                  |                   |      |                |
                       |             +----v------+            |      |    ./app.py    |
                       |             |           |            |      |                |
                       |             |  Routes   |            |      |                |
                       |             |           |            |      |                |
                 eth1  |             |           |            |      +----------------+
                       |             +-----------+            |
                  +----+                                      |
 <------------+   |    |                                      |
                  +----+                                      |
                       |                                      |
                       |                                      |
                       +--------------------------------------+
```


----------------------------------------------------------------------------------------------------------------

## INPUT

<br>

```
    NETWORK                         KERNEL SPACE                        USER SPACE

<------------------->  <-------------------------------------->     <------------------>


                       +--------------------------------------+
                 eth0  |                                      |
       1               |                                      |      +----------------+
 +------------>   +----+     PREROUTING                       |      |                |
                  |    +------------------+                   |      |                |
 <------------+   +----+                  |                   |      |                |
                       |             +----v------+            |      |    ./app.py    |
                       |             |           |       2    |      |                |
                       |             |  Routes   | Non        |      |                |
                       |             |           +------------------->                |
                 eth1  |             |           |            |      +----------------+
                       |             +-----------+    INPUT   |
                  +----+                                      |
 <------------+   |    |                                      |
                  +----+                                      |
                       |                                      |
                       |                                      |
                       +--------------------------------------+
```

----------------------------------------------------------------------------------------------------------------

## OUTPUT

<br>


```
    NETWORK                         KERNEL SPACE                        USER SPACE

<------------------->  <-------------------------------------->     <------------------>


                       +--------------------------------------+
                 eth0  |                                      |
       1               |                            OUTPUT  3 |      +----------------+
 +------------>   +-----------------------+                   |      |                |
                <----------------------------------------------------+                |
 <------------+   +----+                  |                   |      |                |
                       |  PREROUTING +----v------+            |      |    ./app.py    |
                       |             |           |       2    |      |                |
                       |             |  Routes   | Non        |      |                |
                       |             |           +------------------->                |
                 eth1  |             |           |            |      +----------------+
                       |             +-----------+    INPUT   |
                  +----+                                      |
 <------------+   |    |                                      |
                  +----+                                      |
                       |                                      |
                       |                                      |
                       +--------------------------------------+
```

----------------------------------------------------------------------------------------------------------------

## POSTROUTING

<br>

```
    NETWORK                         KERNEL SPACE                        USER SPACE

<------------------->  <-------------------------------------->     <------------------>


                       +--------------------------------------+
                 eth0  |                                      |
       1               |                            OUTPUT  3 |      +----------------+
 +------------>   +-----------------------+                   |      |                |
                <----------------------------------------------------+                |
 <------------+   +----+                  |                   |      |                |
                       |  PREROUTING +----v------+            |      |    ./app.py    |
                       |             |           |       2    |      |                |
                       |             |  Routes   | Non        |      |                |
                       |             |           +------------------->                |
                 eth1  |             |           |            |      +----------------+
                       |             +-----+-----+    INPUT   |
                  +----+                   |   Oui            |
 <------------+  <-------------------------+                  |
                  +----+                                      |
                       |      POSTROUTING                     |
                       |                                      |
                       +--------------------------------------+
```

