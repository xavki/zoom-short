

# TELETRAVAIL : Partage d'expérience


<br>

CHOIX DE POSTE/MISSION AVEC TT

<br>

		* pas de recherche spécifique

<br>

		* pas dans mes critères prioritaires

<br>

		* pratique des entreprises/équipes

-------------------------------------------------------------------------------------

# TELETRAVAIL : Partage d'expérience

<br>

		* entreprises sécurisées = galère ??!

<br>

		* type d'équipe : feature/squad ou même métier
			(important pour la progression et organisation)

<br>

		* souplesse des horaires oui mais...

-------------------------------------------------------------------------------------

# TELETRAVAIL : Partage d'expérience

<br>

PRATIQUE

<br>

		* garder un cyle de journée standard

<br>

		* méfiance et bien boucler la journée

<br>

		* avoir un environnement séparé (2 laptops)

<br>

		* prendre le temps de manger le midi

<br>

		* ne pas se décaler (sommeil & cerveau)

-------------------------------------------------------------------------------------

# TELETRAVAIL : Partage d'expérience

<br>

		* commencer plus tôt mais par une veille plus calme

<br>

		* ne pas oublier les meet "face à face"

<br>

		* profiter des retours bureau pour les sujets plus débatus

<br>

		* manger entre collègues quand on le peut

<br>

		* attention lorsque astreinte (plus de risque récup)
