

# OPEX vs CAPEX... & Cloud


<br>

	OPEX (operational expenditure) : dépenses d'exploitation

		* dépense pour assurer le fonctionnnement régulier (consommable)

		* intérêt : réduction de la facture d'impôt

		* souvent récurrente, peut sembler plus onéreuse

		* exemple : location de laptop


<br>

	CAPEX (capital expenditure) : dépenses d'investissement

		* dépense à valeur positive sur le long terme (non consommable)

		* produits à amortissement sur plusieurs années

		* non pris en compte dans le calcul du cash flow (augmente)

		* mais prise en compte dans le free cash flow

		* exemple : achat de laptop

<br>

Problématiques:

		* achat vs location

		* prise en compte par les investisseurs

		* on premise vs cloud

		* prise en compte de la nécessité sur la durée


