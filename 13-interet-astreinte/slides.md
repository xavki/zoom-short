

# L'ASTREINTE : mon retour d'expérience


<br>

Expérience légère :

		* 3 jobs

		* des astreintes variées

		* 1 femme et 1 enfant

		* problème de sommeil/stress

----------------------------------------------------------------------------------------------

# L'ASTREINTE : mon retour d'expérience


<br>

J'aime l'astreinte pour :

<br>

		* implication dans une société

<br>

		* nécessité à découvrir d'autres périmètres

<br>

		* se retrouver face au mur

<br>

		* l'état d'esprit dans une société

<br>

		* réduire les alertes / meilleure gestion

<br>

		* changement de l'angle de vue

----------------------------------------------------------------------------------------------

# L'ASTREINTE : mon retour d'expérience

<br>

J'aime pas :

<br>

		* perturbation de l'organisation

<br>

		* perturbation de mon organisme

<br>

		* impact important sur le weekend

<br>

		* la petite montée d'adrénaline

<br>

		* même si il y a rien je doute...

<br>

		* organisation familiale

----------------------------------------------------------------------------------------------

# L'ASTREINTE : mon retour d'expérience

<br>

Mon organisation :

<br>

		* une chambre isolée (j'ai de la chance)

<br>

		* le laptop au bout du lit (position nuque)

<br>

		* alerte tel et laptop (régler stopper)

<br>

		* récupération à respecter + rémunération

<br>

		* debrief chaque fin d'astreinte

<br>

		* suivi de toutes les astreintes (quoi, comment...)

<br>

		* le wiki crucial

<br>

		* maintien des heures ordinaires (si possible)
