

# /PROC est ton ami !!!!


<br>

* hello world

```
#include <stdio.h>
int main() {
   int i;

   for ( i = 0; i <= 1000000000000000; i++){
   printf("%d Hello, xavki!\n",i);
   }
   return 0;
}
```

----------------------------------------------------

# /PROC est ton ami !!!!


Compilation :

```
gcc hello-xavki.c -o hello-xavki
```

<br>

```
ps aux  | grep [h]ello
pidof hello-xavki
```

```
lsof -p $(pidof hello-xavki)
```

```
readlink /proc/9168/exe
ls -i /proc/9168/exe
```

```
cp /proc/9168/exe playground/test/xavki
```
