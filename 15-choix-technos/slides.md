

# CHOIX D'UNE TECHNO ??


<br>

Différents cas de figures:

<br>

		* un besoin précis avec délais court ou long

<br>

		* un développeur à trouver un techno utile

<br>

		* compléter son infrastructure (offre int/ext, CI/CD...)

<br>

		* besoin de se mettre à jour (conteneurisation...)

-------------------------------------------------------------------------------------------------------

# CHOIX D'UNE TECHNO ??

<br>

L'environnement :

<br>

		* profile de la société : tech ou pas, poids de l'IT

<br>

		* l'agilité et le process de changement

<br>

		* les ressources humaines disponibles

<br>

		* les compétences/capactiés disponibles (avoir les profiles)

<br>

		* le goût de la prise de risque...

-------------------------------------------------------------------------------------------------------

# CHOIX D'UNE TECHNO ??

<br>

La THEORIE :

<br>

		* possibilité de réutiliser la techno

<br>

		* intérêts partagés avec d'autres équipes

<br>

		* simplicité technique vs l'apport

<br>

		* la robustesse de la technologie :
				* nombre de contribueurs (qui maintient, une société, le business...)
				* nombre de sociétés l'utilisant
				* la documentation et Q/A
				* accessibilité à la formation
				* facilité dans le recrutement (attire ou présence régulière CV)

<br>

		* compétences accessibles à tous (limiter le risque)

<br>

		* la Haute Dispo, scalabilité, la maintenance (distribuée...)

-------------------------------------------------------------------------------------------------------

# CHOIX D'UNE TECHNO ??

<br>

La VRAIE VIE :

<br>

		* peu de temps et de ressources

<br>

		* limitation des choix (raisons tech ou temps ou autres)
<br>

		* le sembalnt de choix (techno déjà fléchée c'est bien mais...)

<br>

		* l'incapacité à revenir sur un choix (reconnaître une erreur, temps...)

<br>

		* l'humain et la divergence des caractères...


	=> le poids des facteurs techniques...
			peut vite être rattrapé par les facteurs humains

-------------------------------------------------------------------------------------------------------

# CHOIX D'UNE TECHNO ??

<br>

Exemple : KUBERNETES vs NOMAD

<br>


		* le business de la techno :

			Kubernetes >  non détenu par à une société

			Nomad > produit signé Hashicorp
				* et même si parfois OSS (qui maintient

-------------------------------------------------------------------------------------------------------

# CHOIX D'UNE TECHNO ??

<br>


		* l'intérêt technique :

			Kubernetes > le plus complet et riche (différents OS, dev K8S...)

			Nomad > moins complet mais plus simple, plus maintenable (mais cf doc...)

-------------------------------------------------------------------------------------------------------

# CHOIX D'UNE TECHNO ??

<br>


		* documentation et formation :

			Kubernetes > y a tout (doc, Q&A, formation, certifs...)	

			Nomad > doc officielle (peu en dehors) et le reste est rare

-------------------------------------------------------------------------------------------------------

# CHOIX D'UNE TECHNO ??

<br>

		
		* le recrutement :

			Kubernetes : plus de monde formés et plus attirant

			Nomad : beaucoup plus rare et difficile process de recrutement

-------------------------------------------------------------------------------------------------------

# CHOIX D'UNE TECHNO ??

<br>


		* plus d'entreprises utilise kubernetes :
				* pas le premier à débuger
				* plus facile à intégrer dans une carrière
				* plus compexe mais plus de formation/doc
				* différentes solutions pour assister le management

	=> on ne parle plus de la même chose presque
