

# AWK - COMMAND SYSTEM


<br>

* exemple kubernetes mais pas d'importance

<br>

* choix de colonne

```
 kubectl get pods -o wide | awk '{print $6}'
```

<br>

* sélection des lignes en fonction d'une colonne

```
kubectl get pods -o wide | awk '($3 ~ "Running") && ($6 ~ "10.233.64.1.*") { print $6}'
```

----------------------------------------------------------------------------------------------------

# AWK - COMMAND SYSTEM

<br>

* passer un ping  

```
kubectl get pods -o wide | awk '($3 ~ "Running") && ($6 ~ "10.233.64.1.*") { print $7}'
```

```
kubectl get pods -o wide | awk '($3 ~ "Running") && ($6 ~ "10.233.64.1.*") {print $1;system("ping -c 1 "$7)}'
```

<br>

* un delete ?

```
kubectl get pods -o wide | awk '($3 ~ "Running") && ($6 ~ "10.233.64.1.*") {print "\n Drop >> "$1;system("kubectl delete pods "$1)}'
```
